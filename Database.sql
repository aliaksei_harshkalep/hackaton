DROP TABLE MenuItems;

CREATE TABLE MenuItems (
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    Name NVARCHAR(256),
    Image NVARCHAR(256),
    Calories INT,
    Rating FLOAT
);

INSERT INTO MenuItems (Name, Image, Calories, Rating) VALUES
    ('Brie cheese on bread', 'Brie_cheese_on_bread.png', 200, 4.5),
    ('Chickpeas with Broccoli and Parmesan', 'Chickpeas_with_Broccoli_and_Parmesan.png', 200, 4.0),
    ('Cinnamon Peach Cottage Cheese Pancake', 'Cinnamon_Peach_Cottage_Cheese_Pancake.png', 200, 5.0),
    ('Cottage Cheese Cantaloupe', 'Cottage_Cheese_Cantaloupe.png', 200, 4.5),
    ('Fried Chicken', 'fried_chicken.png', 200, 4.0),
    ('Fruit salad', 'Fruit_salad.png', 200, 5.0),
    ('Gluten Free Pancakes', 'Gluten_Free_Pancakes.png', 200, 4.5),
    ('Oatmeal banana protein shake', 'Oatmeal_banana_protein_shake.png', 200, 4.0),
    ('Potatoe', 'Potatoe.png', 200, 5.0),
    ('Roast Beef and Avocado Finger Sandwiches', 'Roast_Beef_and_Avocado_Finger_Sandwiches.png', 200, 4.5),
    ('Tuna White Bean Salad', 'Tuna_White_Bean_Salad.png', 200, 4.0),
    ('Veggie Nori Roll', 'Veggie_Nori_Roll.png', 200, 5.0),
    ('Whiskey steak', 'whiskey_steak.png', 200, 4.5);


DROP TABLE Menu;

CREATE TABLE Menu (
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    Name NVARCHAR(256)
);

DROP TABLE MenuMenuItems;

CREATE TABLE MenuMenuItems (
    MenuId INT,
    MenuItemId INT,
    CONSTRAINT PK_MenuMenuItems PRIMARY KEY (MenuId, MenuItemId ASC)
);

INSERT INTO Menu (Name) VALUES
    ('First menu'),
    ('Second menu'),
    ('Third menu'),
    ('Forth menu'),
    ('Fives menu'),
    ('Six menu');

INSERT INTO MenuMenuItems (MenuId, MenuItemId) VALUES
    (1, 1),
    (1, 6),
    (1, 3),
    (2, 4),
    (2, 5),
    (3, 2),
    (3, 7),
    (4, 8),
    (4, 10),
    (5, 9),
    (5, 1),
    (6, 2),
    (6, 12),
    (6, 7);

DROP TABLE UserOrder;

CREATE TABLE UserOrder (
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    UserName NVARCHAR(256),
    OrderDate DATE,
    MenuItemId INT
);
