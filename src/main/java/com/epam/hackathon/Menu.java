package com.epam.hackathon;

import java.io.Serializable;

import lombok.Data;

@Data
public class Menu implements Serializable {
	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
