package com.epam.hackathon;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;


@Data
public class OrderPost implements Serializable {
	private String userName;
	private Date date;
	private List<Integer> menuItemIds;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Integer> getMenuItemIds() {
		return menuItemIds;
	}

	public void setMenuItemIds(List<Integer> menuItemIds) {
		this.menuItemIds = menuItemIds;
	}
}
