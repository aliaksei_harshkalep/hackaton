package com.epam.hackathon;

import java.io.Serializable;
import java.util.Collection;


public class UserOrderResponse implements Serializable {
	private String userName;
	private Collection<MenuItem> menuItems;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Collection<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(Collection<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}
}
