package com.epam.hackathon;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static spark.Spark.*;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.jws.soap.SOAPBinding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Main {
	private static Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws Exception {
		port(4567);
		staticFiles.location("/public");
		staticFiles.expireTime(600L);
		enableCORS("*", "*", "*");

		Class.forName("org.sqlite.JDBC");
		Sql2o sql2o = sql2o = new Sql2o("jdbc:sqlite:sqlite/hackathon.db", "", "");
		try (Connection connection = sql2o.open()) {
			get("/menu", (request, response) -> {
				response.status(200);
				response.type("application/json");
				return dataToJson(getMenuItems(connection, getMenu(connection, Integer.parseInt(request.queryParams("kcal")))));
			});

			get("/order", (request, response) -> {
				response.status(200);
				response.type("application/json");
				List<UserOrder> userOrders = getUserOrders(connection);

				List<UserOrderResponse> userOrderResponses = new ArrayList<UserOrderResponse>();
				String userName = "";
				for (UserOrder userOrder : userOrders) {
					if (userName.equals(userOrder.getUserName())) {
						continue;
					}
					userName = userOrder.getUserName();
					Collection<MenuItem> menuItems = getMenuItems(connection, userOrder.getUserName(), userOrder.getOrderDate());
					UserOrderResponse userOrderResponse = new UserOrderResponse();
					userOrderResponse.setUserName(userName);
					userOrderResponse.setMenuItems(menuItems);
					userOrderResponses.add(userOrderResponse);
				}
				return dataToJson(userOrderResponses);
			});

			post("/order", (request, response) -> {
				try {
					ObjectMapper mapper = new ObjectMapper();
					OrderPost orderPost = mapper.readValue(request.body(), OrderPost.class);
					log.info("Inside [main]: orderPost {}", orderPost);

					response.status(200);
					response.type("application/json");
					return saveOrder(connection, orderPost);
				} catch (Exception e) {
					log.error("Error inside [main]", e);
					response.status(HTTP_BAD_REQUEST);
					return "";
				}
			});
		}
	}

	private static String dataToJson(Object data) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		StringWriter sw = new StringWriter();
		try {
			mapper.writeValue(sw, data);
		}
		catch (Exception e) {
			log.error("Error inside [dataToJson]: data {}", data, e);
		}
		return sw.toString();
	}
	private static Menu getMenu(Connection conn, int kCal) {
		List<Menu> menus = conn.createQuery(
				"select id as id, name as name from"
						+ "(select sum(menuitem.calories) as kCal, menu.id as id, menu.name as name "
						+ "from menumenuitems as item "
						+ "join menu as menu on item.menuId = menu.id "
						+ "join menuitems as menuitem on item.menuItemId = menuitem.id "
						+ "group by menu.id) where kCal <= :kCal").addParameter("kCal", kCal).executeAndFetch(Menu.class);
		log.info("Inside [getMenu]: menus.size {}", menus.size());

		if (menus.size() == 0) {
			return null;
		}
		return menus.get(0);
	}

	private static Collection<MenuItem> getMenuItems(Connection conn, Menu menu) {
		if (menu == null) {
			return Collections.emptyList();
		}
		List<MenuItem> menuItems = conn.createQuery("select t2.id, t2.name, t2.image, t2.calories, t2.rating "
				+ "from menumenuitems as t1 "
				+ "join menuitems t2 on t1.menuitemid = t2.id "
				+ "where t1.menuid = :menuId").addParameter("menuId", menu.getId()).executeAndFetch(MenuItem.class);
		log.info("Inside [getMenuItems]: menuItems.size {}", menuItems.size());

		return menuItems;
	}

	private static int saveOrder(Connection connection, OrderPost orderPost) {
		for (Integer id : orderPost.getMenuItemIds()) {
			connection.createQuery("insert into userorder "
					+ "(username, orderdate, menuitemid) "
					+ "values (:username, :orderdate, :menuitemid)")
					.addParameter("username", orderPost.getUserName())
					.addParameter("orderdate", orderPost.getDate())
					.addParameter("menuitemid", id).executeUpdate();
		}
		return 1;
	}

	private static List<UserOrder> getUserOrders(Connection conn) {
		List<UserOrder> userOrders = conn.createQuery("select userName, orderDate, menuItemId "
				+ "from userorder ").executeAndFetch(UserOrder.class);
		log.info("Inside [getUserOrders]: userOrders.size {}", userOrders.size());

		return userOrders;
	}

	private static Collection<MenuItem> getMenuItems(Connection conn, String userName, Date date) {
		List<MenuItem> menuItems = conn.createQuery("select t1.id, t1.name, t1.image, t1.calories, t1.rating "
				+ "from menuitems as t1 "
				+ "join userorder t2 on t1.id = t2.menuitemid "
				+ "where t2.username = :username and t2.orderDate = :date")
				.addParameter("username", userName)
				.addParameter("date", date).executeAndFetch(MenuItem.class);
		log.info("Inside [getMenuItems]: menuItems.size {}", menuItems.size());

		return menuItems;
	}

	private static void enableCORS(final String origin, final String methods, final String headers) {
		options("/*", (request, response) -> {

			String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null) {
				response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}

			String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null) {
				response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}

			return "OK";
		});

		before((request, response) -> {
			response.header("Access-Control-Allow-Origin", origin);
			response.header("Access-Control-Request-Method", methods);
			response.header("Access-Control-Allow-Headers", headers);
			// Note: this may or may not be necessary in your particular application
			response.type("application/json");
		});
	}
}
